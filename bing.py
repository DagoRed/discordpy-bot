
import os
import requests
import flag
import json 


strongVals = {
	'usconfirmed':0,
	'usrecovered':1,
	'usdead': 2,
	'caconfirmed':3,
	'carecovered':4,
	'cadead': 5,
}

statesbingjson = {}
with open("statesbing.json",'r') as statebingfile:
	statesbingjson = json.load(statebingfile)

#### <BingAPI> ####
def bingOutput(region):
	outFormat = ""
	addFlag = ""
	#separate all values and change any none/null values to 0
	outList = []
	outList.append(region['displayName'])
	outList.append(region['totalConfirmed'])
	outList.append(region['totalConfirmedDelta'])
	outList.append(region['totalDeaths'])
	outList.append(region['totalDeathsDelta'])
	outList.append(region['totalRecovered'])
	outList = [0 if x == None else x for x in outList]
	
	#get flag emoji if global country
	try:
		if region['parentId'] == "world":
			link = "https://flagpedia.net/download/country-codes-case-upper.json"
			request = requests.get(link)
			uniFlags = request.json() 
			for key, value in uniFlags.items():
				if value == outList[0]:
					countryCode = key
			addFlag = flag.flag(countryCode)
	except:
		pass

	#return all values and add commas to number values
	outFormat = f"{addFlag} {outList[0]} Covid-19 Statistics\n"
	outFormat += "🤢 Total Cases: " + str(f"{outList[1]:n}")
	outFormat += " -- New Cases: " + str(f"{outList[2]:n}")
	outFormat += "  💀 Total Deaths: " + str(f"{outList[3]:n}")
	outFormat += " -- New Deaths: " + str(f"{outList[4]:n}")
	outFormat += "  😅 Total Recovered: " + str(f"{outList[5]:n}")
	return outFormat

def usStateStats(state):
	link = "https://bing.com/covid/data"
	request = requests.get(link)
	bingCovid = request.json() 
	stateOut = ""
	getState = state.upper()
	try:
		getState = statesbingjson['states'][(getState[:2])]
		for i in bingCovid['areas'][0]['areas']:
			if i['id'] == getState:
				stateOut = bingOutput(i)
	except:
		stateOut = getState + " is not in the state list.  Try again."
	return stateOut

def usCountyStats(state, county):
	link = "https://bing.com/covid/data"
	request = requests.get(link)
	bingCovid = request.json()
	# bingCovid = requests.get("https://bing.com/covid/data")
	countyOut = ""
	getState = state.upper()
	try:
		getState = statesbingjson['states'][(getState[:2])]
		getCounty = county.lower() + "_" + getState
		for i in bingCovid['areas'][0]['areas']:
			if i['id'] == getState:
				for sc in i['areas']:
					if sc['id'] == getCounty:
						countyOut = bingOutput(sc)
	except:
		countyOut = getCounty + " is not in the county list.  Try again."
	return countyOut

def countryStats(country):
	link = "https://bing.com/covid/data"
	request = requests.get(link)
	bingCovid = request.json() 
	countryOut = ""
	if country == "":
		countryOut = bingOutput(bingCovid)
	else:
		try:
			for i in bingCovid['areas']:
				if i['id'] == country:
					countryOut = bingOutput(i)
		except:
			countryOut = country + " is not in the country list. Try again."
	return countryOut
#### </BingAPI> ####